import { useState } from "react";
import Hero from "./components/Hero";
import Who from "./components/Who";
import Work from "./components/Work";
import Contact from "./components/Contact";
import ThreeBox from "./components/ThreeBox";
import Cube from "./components/Cube";

function App() {
  return (
    <>
      <div
        className="h-screen overflow-y-auto text-white scroll"
        style={{
          backgroundImage: "url('./img/bg.jpeg')",
        }}
      >
        <Hero />
        <Who />
        <Work />
        <Contact />
      </div>
    </>
  );
}

export default App;
