import { OrbitControls, Stage } from "@react-three/drei";
import { Canvas } from "@react-three/fiber";
import React from "react";
import Samsung from "./Samsung";

const Product = () => {
  return (
    <Canvas>
      <OrbitControls scale={2} />
      <Stage environment={"city"} intensity={0.6}>
        <Samsung />
      </Stage>
    </Canvas>
  );
};

export default Product;
