import emailjs from "@emailjs/browser";
import { ChangeEvent, useRef, useState } from "react";

const Form = () => {
  const [success, setSuccess] = useState<boolean>();
  const formRef = useRef<HTMLFormElement>(null);

  const handleSubmit = (e: ChangeEvent<HTMLFormElement>) => {
    e.preventDefault();

    formRef &&
      formRef.current &&
      emailjs
        .sendForm(
          "service_6jdxqw8",
          "template_okxj7pa",
          formRef.current,
          "uCeuFnsrrSQBUR0EO"
        )
        .then((res) => setSuccess(true))
        .catch((err) => {
          console.log(err);
          setSuccess(false);
        });
  };
  return (
    <form
      ref={formRef}
      onSubmit={handleSubmit}
      className="flex w-[500px] flex-col gap-6"
    >
      <h1 className="font-[200px]">Contact Us</h1>
      <input
        className="p-5 text-slate-950 outline-none bg-gray-300 border-none rounded-lg"
        type="text"
        placeholder="Name"
        name="name"
      />
      <input
        className="p-5 text-slate-950 outline-none bg-gray-300 border-none rounded-lg"
        type="text"
        name="email"
        placeholder="Email"
      />
      <textarea
        className="p-5 text-slate-950 outline-none bg-gray-300 border-none rounded-lg"
        name="message"
        id=""
        rows={10}
        placeholder="Write your message"
      />
      <button
        className="p-4 font-bold text-white bg-pink-400 rounded-lg "
        type="submit"
      >
        send
      </button>
      {success &&
        "Your message has been sent. We'll get back to you soon:)))))))))"}
    </form>
  );
};

export default Form;
