import { Canvas } from "@react-three/fiber";
import Navbar from "./Navbar";
import { MeshDistortMaterial, OrbitControls, Sphere } from "@react-three/drei";

const Hero = () => {
  return (
    <div className="flex flex-col items-center h-screen scroll-align-center">
      <Navbar />
      <div className="flex w-3/4 h-full scroll-align-center ">
        <div className="flex-[2] h-full justify-center  flex flex-col items-start gap-5">
          <h1 className="text-7xl">Think. Make. Solve</h1>
          <div className="flex items-center gap-5">
            <img src="./img/line.png" className="h-1" alt="" />
            <h2 className="text-pink-500">What's Se DO</h2>
          </div>
          <p className="text-2xl text-gray-500">
            we enjoy creating delightful, human-centered digital experience
          </p>
          <button className="p-4 text-white bg-pink-400 rounded-lg">
            Learn more
          </button>
        </div>
        {/* Right side */}
        <div className="flex-[3] relative h-full">
          {/* 3d model */}
          <Canvas>
            <OrbitControls enableZoom={false} />
            <ambientLight intensity={1} />
            <directionalLight position={[2, 2, 1]} />
            <Sphere args={[1, 100, 200]} scale={2}>
              <MeshDistortMaterial
                color={"#412f4e"}
                attach={"material"}
                distort={0.5}
                speed={2}
              />
            </Sphere>
            {/* image */}
          </Canvas>
          <img
            src="./img/moon.png"
            className="absolute top-0 bottom-0 left-0 right-0 object-contain w-3/4 m-auto hero-animate h-3/4"
            alt=""
          />
        </div>
      </div>
    </div>
  );
};

export default Hero;
