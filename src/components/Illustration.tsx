import { OrbitControls, Stage } from "@react-three/drei";
import { Canvas } from "@react-three/fiber";
import Harry from "./Harry";
const Illustration = () => {
  return (
    <Canvas>
      <OrbitControls autoRotate />
      <Stage environment={"city"} intensity={0.6}>
        <Harry />
      </Stage>
    </Canvas>
  );
};

export default Illustration;
