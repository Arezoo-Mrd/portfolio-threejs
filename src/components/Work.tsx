import { useState } from "react";
import WebDesign from "./WebDesign";
import Product from "./Product";
import Development from "./Development";
import SocialMedia from "./SocialMedia";
import Illustration from "./Illustration";

const Work = () => {
  const [work, setWork] = useState("Web Design");
  return (
    <>
      <div className="flex items-center justify-center h-screen scroll-align-center">
        <div className="flex justify-between w-3/4 ">
          {/* Left sidebar */}
          <div className="flex items-center flex-1">
            <ul className="flex flex-col gap-5">
              <li
                className={`font-bold text-transparent relative cursor-pointer text-6xl text-stroke
                        after:absolute dynamic-content after:top-0 after:left-0 hover:after:text-white after:content-['Web_Design']  after:w-0 after:overflow-hidden after:whitespace-nowrap animate-moveText`}
                onClick={() => setWork("Web Design")}
              >
                Web Design
              </li>
              <li
                className={`font-bold text-transparent relative cursor-pointer text-6xl text-stroke
                        after:absolute dynamic-content after:top-0 after:left-0 hover:after:text-white after:content-['Development'] after:w-0 after:overflow-hidden after:whitespace-nowrap animate-moveText `}
                onClick={() => setWork("Development")}
              >
                Development
              </li>
              <li
                className={`font-bold text-transparent relative cursor-pointer text-6xl text-stroke
                        after:absolute dynamic-content after:top-0 after:left-0 hover:after:text-white after:content-['Illustration']  after:w-0 after:overflow-hidden after:whitespace-nowrap animate-moveText`}
                onClick={() => setWork("Illustration")}
              >
                Illustration
              </li>
              <li
                className={`font-bold text-transparent relative cursor-pointer text-6xl text-stroke
                        after:absolute dynamic-content after:top-0 after:left-0 hover:after:text-white after:content-['Product_Design'] after:w-0 after:overflow-hidden after:whitespace-nowrap animate-moveText `}
                onClick={() => setWork("Product Design")}
              >
                Product Design
              </li>
              <li
                className={`font-bold text-transparent relative cursor-pointer text-6xl text-stroke
                        after:absolute dynamic-content after:top-0 after:left-0 hover:after:text-white after:content-['Social_Media'] after:w-0 after:overflow-hidden after:whitespace-nowrap animate-moveText `}
                onClick={() => setWork("Social Media")}
              >
                Social Media
              </li>
            </ul>
          </div>
          {/* Right sidebar */}
          <div className="flex-1">
            {work === "Web Design" ? (
              <WebDesign />
            ) : work === "Development" ? (
              <Development />
            ) : work === "Social Media" ? (
              <SocialMedia />
            ) : work === "Illustration" ? (
              <Illustration />
            ) : (
              <Product />
            )}
          </div>
        </div>
      </div>
    </>
  );
};

export default Work;
