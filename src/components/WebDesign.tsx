import { OrbitControls, Stage } from "@react-three/drei";
import { Canvas } from "@react-three/fiber";
import Laptop from "./Laptop";
import { useRef } from "react";

const WebDesign = () => {
  const ref = useRef(null);
  return (
    <Canvas ref={ref}>
      <OrbitControls scale={5} enableZoom={false} />
      <Stage environment={"city"} intensity={1.6}>
        <Laptop />
      </Stage>
    </Canvas>
  );
};

export default WebDesign;
