import { Canvas } from "@react-three/fiber";
import { OrbitControls } from "@react-three/drei";
import Cube from "./Cube";
const Who = () => {
  return (
    <div className="flex items-center justify-center h-screen scroll-align-center">
      <div className="flex w-full h-full scroll-align-center ">
        <div className="flex-1 h-full">
          {/* 3d model */}
          <Canvas
            camera={{
              fov: 25,
              position: [5, 5, 5],
            }}
          >
            <OrbitControls enableZoom={false} autoRotate />
            <ambientLight intensity={1} />
            <directionalLight position={[2, 2, 1]} />
            <Cube />
          </Canvas>
        </div>
        {/* Right side */}
        <div className="relative flex flex-col items-start justify-center flex-1 h-full gap-5">
          <h1 className="text-5xl">Think outside the square space</h1>
          <div className="flex items-center gap-5">
            <img src="./img/line.png" className="h-1" alt="" />
            <h2 className="text-pink-500">Who We Are</h2>
          </div>
          <p className="text-2xl text-gray-500">
            a create group of designers and developers whits a passion for the
            arts.
          </p>
          <button className="p-4 text-white bg-pink-400 rounded-lg ">
            see our works
          </button>
        </div>
      </div>
    </div>
  );
};

export default Who;
