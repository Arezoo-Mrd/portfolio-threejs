import { ChangeEvent } from "react";
import MapChart from "./MapChart";
import Form from "./Form";

const Contact = () => {
  return (
    <div className="h-screen scroll-align-center">
      <div className="flex justify-between w-full h-full gap-14">
        {/* Left Sidebar */}
        <div className="flex items-center justify-end flex-1">
          <Form />
        </div>
        {/* Right Sidebar */}
        <div className="flex-1">
          <MapChart />
        </div>
      </div>
    </div>
  );
};

export default Contact;
