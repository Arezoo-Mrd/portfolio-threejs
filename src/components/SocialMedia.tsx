import { OrbitControls, Stage } from "@react-three/drei";
import { Canvas } from "@react-three/fiber";
import Social from "./Social";

const SocialMedia = () => {
  return (
    <Canvas>
      <OrbitControls />
      <Stage environment={"city"} intensity={0.6}>
        <Social />
      </Stage>
    </Canvas>
  );
};

export default SocialMedia;
