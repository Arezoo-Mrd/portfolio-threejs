import { Canvas } from "@react-three/fiber";
import {
  OrbitControls,
  RenderTexture,
  Text,
  PerspectiveCamera,
} from "@react-three/drei";
import Cube from "./Cube";

const ThreeBox = () => {
  return (
    <div className="w-full h-screen scroll-align-center">
      <Canvas>
        <OrbitControls enableZoom={false} autoRotate />
        <ambientLight intensity={1} />
        <directionalLight position={[3, 2, 1]} />
        {/* <mesh>
          <boxGeometry args={[2, 2, 2]} />
          <meshStandardMaterial>
            <RenderTexture sourceFile={undefined} attach={"map"}>
              <PerspectiveCamera makeDefault position={[0, 0, 5]} />
              <color attach={"background"} args={["#dc9dcd  "]} />
              <Text textAlign="center" fontSize={3} color={"#555"}>
                hello
              </Text>
            </RenderTexture>
          </meshStandardMaterial>
        </mesh> */}
        <Cube />
      </Canvas>
    </div>
  );
};

export default ThreeBox;
