const Navbar = () => {
  return (
    <div className="flex justify-center w-full">
      <div className="flex items-center justify-between w-3/4 py-3 ">
        <div className="flex items-center gap-5">
          <img className="h-14" src="./img/logo.png" />
          <ul className="flex gap-5 ">
            <li className="cursor-pointer">Home</li>
            <li className="cursor-pointer">Studio</li>
            <li className="cursor-pointer">works</li>
            <li className="cursor-pointer">Contacts</li>
            <li className="cursor-pointer"></li>
          </ul>
        </div>
        {/* Icons */}
        <div className="flex items-center gap-5">
          <img className="w-5 cursor-pointer" src="./img/search.png" alt="" />
          <button className="p-4 text-white bg-pink-400 rounded-lg w-28">
            Hire Now
          </button>
        </div>
      </div>
    </div>
  );
};

export default Navbar;
